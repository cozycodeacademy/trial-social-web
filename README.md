# 交友網頁程式體驗 Cozy Code Academy
[瀏覽官方網頁了解詳情](https://cozycode.co/courses/web-application)
# 如何使用
1. [下載程式碼](https://gitlab.com/cozycodeacademy/trial-social-web)
2. 使用瀏覽器打開demo.html試用製成品
3. 使用Microsoft Visual Code打開exercise.html開始編寫程式
4. 按exercise.html內提供的function完成網頁
 
# Social Web Trial Class - Cozy Code Academy
[Learn more the official website](https://cozycode.co/courses/web-application)
# How to use
1. [Download the project](https://gitlab.com/cozycodeacademy/trial-social-web)
2. Use browser to open demo.html and try the feature
3. Use Microsoft Visual Code to open exercise.html, start your coding
4. Finish the exercise.html using the functions provided in HTML file
